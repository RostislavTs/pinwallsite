
class Pinwall_heder_el{
    constructor(id, el_text, images, add_button, button_text){
        this.element_text = el_text;
        this.element_images = images;
        this.addButton = add_button;
        if(add_button == true){
            this.element_button_text = button_text;
        }
        this.uid = id;
    }

    image_lenght(){
        return this.element_images.lenght;
    }

    getImageSrc(id_image){
        if(id_image < this.image_lenght()){
            return this.element_images[id_image];
        }
    }

    get images(){
        return this.element_images;
    }

    get text(){
        return this.element_text;
    }

    get button_text(){
        return this.element_button_text;
    }

    get isAddButton(){
        return this.addButton;
    }

    get id(){
        return this.uid;
    }

    generateHtml(){
        let html_code = '<ul>';
        for(let i in this.element_images){
            html_code += `<li><img src = "${this.element_images[i]}" /></li>`;
        }
        html_code += '</ul>';
        html_code += `<span>${this.element_text}</span>`;
        if(this.addButton == true){
            html_code += `<button>${this.element_button_text}</button>`;
        }
        return html_code;
    }
}





export default Pinwall_heder_el;